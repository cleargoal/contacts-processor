<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use stdClass;

class ProcessService
{
    /** $bearer */
    private string $bearer;

    /** Src Data from file */
    private array $srcFromFile;

    /** Found in CRM unique */
    private array $foundInCrmUniqueSer;

    /** Missing in CRM */
    private array $missingInCrm;

    /**
     * Read src file
     *
     * @param
     *
     * @return array
     */
    public function readSrcFile(): array
    {
        $srcName = Storage::get('current_file.txt');
        $rawArray = Excel::toArray(new stdClass(), $srcName);
        $dataArray = [];

        $allHeaders = $rawArray[0][0];
        $phoneHeaders = collect($allHeaders)->filter(function ($item, int $key) {
            return Str::contains($item, ['phone', 'ref1', 'tel1']);
        });

        foreach ($rawArray[0] as $key => $rowData) {
            if ($key === 0) {
                continue;
            }

            $foundPhone = '';
            foreach ($phoneHeaders as $fieldKey => $field) {
                if (!empty($rowData[$fieldKey])) {
                    $foundPhone = $rowData[$fieldKey];
                    break;
                }
            }

                $dataArray[] = [
                    'campaign' => $rowData[1],
                    'name' => $rowData[2],
                    'email' => $rowData[3],
                    'phone' => $foundPhone,
                ];
        }

        Storage::put('ready_contacts.txt', serialize($dataArray));
        Storage::delete($srcName);

        return ['text' => 'Файл з контактами прочитано успішно.', 'color' => '', 'next' => true];
    }

    /**
     * Check Contacts In Crm
     */
    public function checkContactsInCrm(): array
    {
        $crmQueryData = $this->crmQueryData(); // array of strings compiled of emails and phones, to fit query limit in 'Where' case - 50 items
        try {
            $crmFound = $this->queryingCrm($crmQueryData);
        } catch (\Exception $ex) {
            return [
                'text' => 'Помилка мережі або на сервері. Спробуйте ще раз. У разі повторної помилки спробуйте пізніше.',
                'color' => 'repeat',
                'next' => false,
                'exception' => $ex
            ];
        }
        $missing = $this->missingContactsInCrm();

        return ['text' => 'Контакти в Зохо перевірено. Відсутні - ' . $missing, 'color' => '', 'next' => $missing > 0, 'exist' => $crmFound];
    }

    /**
     * Authorize. Refresh access_token
     */
    protected function authorize(): string
    {
        $response = Http::asForm()->post(config('zoho.auth_url'), [
            'refresh_token' => config('zoho.refresh_token'),
            'client_id' => config('zoho.client_id'),
            'client_secret' => config('zoho.client_secret'),
            'grant_type' => 'refresh_token',
        ]);

        return $response->json($key = 'access_token');
    }

    /**
     * Data for CRM Query
     *
     * @return array
     */
    protected function crmQueryData(): array
    {
        $this->srcFromFile = unserialize(Storage::get('ready_contacts.txt'));
        $emails = array_column($this->srcFromFile, 'email');
        $emails = $this->transformColumn($emails, 'Email');

//        $refs = array_column($this->srcFromFile, 'ref1');
//        $tels = array_column($this->srcFromFile, 'tel1');
        $phones = array_column($this->srcFromFile, 'phone');

        $phones = $this->transformColumn($phones, 'Phone');

        return array_merge($emails, $phones);
    }

    /**
     * Clean Columns
     */
    protected function transformColumn($data, $key): array
    {
        $filtered = array_filter($data);
        $quoted = array_map(function ($item) {
            return "'" . $item . "'";
        }, $filtered);
        $chunked = array_chunk($quoted, 50);
        $imploded = [];
        foreach ($chunked as $item) {
            $imploded[] = [$key, implode(',', $item)];
        }
        return $imploded;
    }

    /**
     * Querying CRM
     *
     * @param
     *
     * @return array
     */
    protected function queryingCrm($crmQueryData): array
    {
        $this->bearer = $this->authorize();
        Storage::put('bearer.txt', $this->bearer);

        $fields = [
            'Email' => ['Email', 'Secondary_Email', 'Email2', 'field34'], // for fields: Email', 'Доп. адрес эл. почты', 'Доп Email', 'Эл. почта 3'
            'Phone' => ['Phone', 'Home_Phone', 'Mobile', 'Other_Phone', 'field56'], // // for fields: 'Phone', 'Домашний телефон', 'Моб. телефон', 'Другой номер телефона', 'Доп_Телефон'
        ];
        $foundInCrmStack = [];

        foreach ($crmQueryData as $data) {
            foreach ($fields[$data[0]] as $fKey => $queryKey) {
                if ($data[0] === 'Email') {
                    $queryBase = 'select ' . $queryKey . ' as Email, Last_Name, First_Name, Phone, field36 from Contacts where ' . $queryKey . ' in (' . $data[1] . ')';
                }
                else /*($queryKey === 'Phone')*/ {
                    $queryBase = 'select Email, Last_Name, First_Name, ' . $queryKey . ' as  Phone, field36 from Contacts where ' . $queryKey . ' in (' . $data[1] . ')';
                }

                $foundInCrmStack[] = Http::withToken($this->bearer)
                    ->post(
                        config('zoho.coql_url'),
                        [
                            'select_query' => $queryBase,
                        ]
                    )->json();
            }
        }

        $allDataFound = [];
        foreach ($foundInCrmStack as $found) {
            if ($found && isset($found['data'])) {
                foreach ($found['data'] as $fd) {
                    $allDataFound[] = serialize($fd);
                }
            }
        }

        $this->foundInCrmUniqueSer = array_unique($allDataFound);

        return $this->prepareExistForReport();
    }

    /**
     * Separate Contacts missing in CRM
     *
     * @return int
     */
    protected function missingContactsInCrm(): int
    {
        foreach ($this->foundInCrmUniqueSer as $found) {
            foreach ($this->srcFromFile as $key => $src) {

                if (str_contains($found, $src['email'])) {
                    unset($this->srcFromFile[$key]);
                    break;
                }

                if (str_contains($found, $src['phone'])) {
                    unset($this->srcFromFile[$key]);
                    break;
                }
            }
        }

        // TODO: try to save 'missing' to CSV
//        if (count($this->srcFromFile) > 0) {
//            $handle = fopen('missing.csv', 'w');
//
//            foreach ($data as $row) {
//                fputcsv($handle, $row);
//            }
//
//            fclose($handle);
//            Storage::put('missing.csv', $this->srcFromFile);
//        }

        Storage::put('missing.txt', serialize($this->srcFromFile));

        return count($this->srcFromFile);
    }


    /** ***  Separated Stream  ***  */
    /**
     * Create in CRM missing Contacts
     *
     * @return array
     */
    public function createContactsInCrm(): array
    {
        $this->bearer = Storage::get('bearer.txt');
        $missing = unserialize(Storage::get('missing.txt'));
        $recordArray = [];

        foreach ($missing as $contact) {
            $recordObject["Last_Name"] = $contact['name'];
            $recordObject["Email"] = $contact['email'];
            $recordObject["Description"] = "Came from automatic creation by V.Y.";
            $recordObject["Phone"] = $contact['phone'];
            $recordObject["field36"] = $contact['campaign'];

            $recordArray[] = $recordObject;
        }
        $requestBody["data"] = $recordArray;

        $insResult = Http::withToken($this->bearer)
            ->post(
                config('zoho.insert_url'),
                $requestBody
            )->json();

        return ['text' => 'Контакти створені.', 'color' => '', 'next' => true, 'missing' => $missing];
    }

    /**
     * Prepare exist contacts for report
     *
     * @return array $reportExist
     */
    protected function prepareExistForReport(): array
    {
        $reportExist = [];
        foreach ($this->foundInCrmUniqueSer as $item) {
            $unser = unserialize($item);
            $reportExist[] = [
                'name' => $unser['Last_Name'] . ' ' . $unser['First_Name'],
                'email' => $unser['Email'],
                'phone' => $unser['Phone'],
                'campaign' => $unser['field36'],
            ];
        }
        return $reportExist;
    }
}