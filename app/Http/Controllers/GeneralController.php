<?php

namespace App\Http\Controllers;

use App\Services\ProcessService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GeneralController
{

    /**
     * upload Src File
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function uploadSrcFile(Request $request): JsonResponse
    {
        $path = null;
        if ($request->file) {
            $path = $request->file('file')->store('files');
        }
        if ($path) {
            Storage::put('current_file.txt', $path);
            $response = ['text' => 'Файл завантажено успішно.', 'color' => '', 'next' => true];
        } else {
            $response = [
                'text' => 'Не вдалося завантажити файл. Спробуйте ще раз. У разі повторної неможливості зверніться до програміста.'
                , 'color' => '',
                'next' => false
            ];
        }
        return response()->json($response);
    }

    /**
     * Read Src file
     *
     * @param ProcessService $service
     *
     * @return JsonResponse
     */
    public function readSrcFile(ProcessService $service): JsonResponse
    {
        $res = $service->readSrcFile();
        return response()->json($res);
    }

    /**
     * Check found contacts in CRM
     *
     * @param ProcessService $service
     *
     * @return JsonResponse
     */
    public function checkInCrm(ProcessService $service): JsonResponse
    {
        $res = $service->checkContactsInCrm();
        return response()->json($res);
    }

    /**
     * Create insufficient contacts in CRM
     *
     * @param ProcessService $service
     *
     * @return JsonResponse
     */
    public function createInCrm(ProcessService $service): JsonResponse
    {
        $res = $service->createContactsInCrm();
        return response()->json($res);
    }
    
}