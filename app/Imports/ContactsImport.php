<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;

class ContactsImport implements ToCollection
{
    use Importable;
    /**
     * @param Collection $collection
     *
     * @return array
     */
    public function collection(Collection $collection)
    {
        Log::info('file import', [$collection]);
//        return $srcCont = [
//            'name'     => $row[0],
//            'email'    => $row[1],
//            'password' => Hash::make($row[2]),
//        ];
    }
}
