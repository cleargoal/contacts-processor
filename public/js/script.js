const processMessage = document.querySelector('#proc-mess');
const choseFile = document.querySelector('#chose-file');
const form = document.querySelector('form');
const subBut = document.querySelector('#submit');
const preloader = document.querySelector('#preloader');
const leftLighter = document.querySelector('#left');
const rightLighter = document.querySelector('#right');
const reportNew = document.querySelector('#new-contacts');
const reportExist = document.querySelector('#exist-contacts');
let blinkInterval = null;

const colors = {
    repeat: 'text-blue',
};

subBut.addEventListener('click', handleSubmit);

choseFile.addEventListener('change', function (ev) {
    messages({text: 'Завантаження файлу ... Натисніть кнопку обробки.'});
    subBut.classList.remove('hidden');
});

function messages(mess) {
    let newLi = document.createElement('li');
    if (mess.color ) {
        newLi.classList.add(colors[mess.color]);
    }
    newLi.innerText = mess.text;
    processMessage.appendChild(newLi);
}

function clearMessages() {
    processMessage.innerHTML = '';
}

async function handleSubmit(event) {
    event.preventDefault();
    clearMessages();
    messages({text: 'Виконую.'});
    showPreloader();
    blink();
    await upload();
    await read();
    const checked = await check();
    checked.next === true ? await create() : '';
    showPreloader();
    messages({text: 'Завершено.'});
}

async function upload() {
    messages({text: 'Завантажуємо файл.'})
    const url = new URL("/upload", document.URL);
    const formData = new FormData();
    formData.append('file', form.file.files[0]);
    const csrf = form._token.value;

    const fetchOptions = {
        method: form.method,
        body: formData,
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'Accept':  'application/json',
            'X-CSRF-TOKEN': csrf,
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    };

    const res = await fetch(url, fetchOptions);
    let resJson = await res.json();
    messages(resJson) ;
    return resJson;
}

async function read() {
    messages({text: 'Читання контактів з файлу та видалення дублікатів.'});
    const url = new URL("/read", document.URL);
    const res = await fetch(url);
    messages(await res.json());
}

async function check() {
    messages({text: 'Перевірка наявності контактів у СРМ.'});
    const url = new URL("/check-crm", document.URL);
    const res = await fetch(url);
    let resJson = await res.json();
    messages(resJson);
    if (resJson.exist) {
        showReport(resJson.exist, reportExist);
    }
    if (resJson.exception) {
    }
    return resJson;
}

async function create() {
    messages({text: 'Створення відсутніх контактів у СРМ.'});
    const url = new URL("/create-crm", document.URL);
    const res = await fetch(url);
    let resJson = await res.json();
    messages(resJson);
    if (resJson.missing) {
        showReport(resJson.missing, reportNew);
    }
}

function blink() {
    blinkInterval = setInterval(function () {
        leftLighter.classList.toggle('lighter-light');
        leftLighter.classList.toggle('lighter-green');
        rightLighter.classList.toggle('lighter-light');
        rightLighter.classList.toggle('lighter-green');
    }, 500);
}

function showPreloader() {
    preloader.classList.toggle('hidden');
}

function showReport(missing, area) {
    const missArray = Object.values(missing);
    missArray.forEach((elem, index) => {
        let newLi = document.createElement('li');
        newLi.innerHTML = '<span>' + elem.name + '</span><span>' + elem.email + '</span><span>' + elem.phone + '</span><span>' + elem.campaign + '</span>';
        area.appendChild(newLi);
    });

}