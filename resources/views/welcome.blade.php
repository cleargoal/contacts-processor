<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Shaia Contacts</title>
    <link rel="icon" href="{{asset('neunuhr.png')}}">

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet"/>

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('/css/app.css')}}">
</head>
<body class="antialiased">
<div class="relative sm:flex sm:justify-center min-h-screen bg-dots-darker bg-center bg-gray-100 dark:bg-dots-lighter dark:bg-gray-900 selection:bg-red-500 selection:text-white">
    @if (Route::has('login'))
        <div class="sm:fixed sm:top-0 sm:right-0 p-6 text-right z-10">
            @auth
                <a href="{{ url('/home') }}"
                   class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Home</a>
            @else
                <a href="{{ route('login') }}"
                   class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Log
                    in</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}"
                       class="ml-4 font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Register</a>
                @endif
            @endauth
        </div>
    @endif

    <div class="max-w-7xl mx-auto p-6 lg:p-8">
        <div class="flex justify-center">
            <h1>ШАЯ перевірка наявності контактів у Зохо</h1>
        </div>

        <div class="mt-8">
            <div class="grid grid-cols-1 gap-6 lg:gap-8">
                <div class="scale-100 p-6 bg-white dark:bg-gray-800/50 dark:bg-gradient-to-bl from-gray-700/50 via-transparent dark:ring-1
                        dark:ring-inset dark:ring-white/5 rounded-lg shadow-2xl shadow-gray-500/20 dark:shadow-none flex transition-all duration-250 focus:outline focus:outline-2 focus:outline-red-500">
                    <div>
                        <h2 class="mt-6 text-xl font-semibold text-gray-900 dark:text-white">Завантажте файл</h2>

                        <div class="mt-4 text-gray-500 dark:text-gray-400 text-sm leading-relaxed">
                            <form method="post" enctype="multipart/form-data">
                                @csrf
                                <label for="chose-file" class="mr-1">Обрати файл</label>
                                <input type="file" id="chose-file" name="file" class="mb-4">
                                <button id="submit" class="bg-dots-darker bg-gray-100 p-4 border min-w-but hidden">Обробити!</button>
                            </form>
                        </div>
                    </div>

                </div>

                <div class="rounded-lg bg-brown-150 p-4 flex px-6">
                    <div class="min-w-but">
                        <div id="preloader" class="rounded-full preloader hidden">
                            <div class="flex lighter-wrapper">
                                <span id="left" class="rounded-full lighter lighter-light"></span>
                                <span id="right" class="rounded-full lighter lighter-green"></span>
                            </div>
                            <div class="text-gray-50 font-semibold">Працюю ...</div>
                        </div>
                    </div>
                    <ol id="proc-mess" class="text-xl text-gray-50 px-6"></ol>
                </div>

                <div
                    id="report"
                    class="scale-100 px-6 pb-6 bg-white dark:bg-gray-800/50 dark:bg-gradient-to-bl from-gray-700/50 via-transparent dark:ring-1 dark:ring-inset dark:ring-white/5
                    rounded-lg shadow-2xl shadow-gray-500/20 dark:shadow-none flex transition-all duration-250 focus:outline focus:outline-2 focus:outline-red-500"
                >
                    <div>
                        <h4 class="text-gray-600 dark:text-white">Звіт</h4>
                        <div >
                            <h5>Нові - не знайдено в Зохо, додали</h5>
                            <ol id="new-contacts" class="custom-list">
                                <li class="flex sm:justify-between mb-2">
                                    <span>Ім'я</span>
                                    <span>Емейл</span>
                                    <span>Телефон</span>
                                    <span>Кампанія</span>
                                </li>
                            </ol>
                        </div>

                        <div>
                            <h5>Існуючі - знайдено в Зохо</h5>
                            <ol id="exist-contacts" class="custom-list">
                                <li class="flex sm:justify-between mb-2">
                                    <span>Ім'я</span>
                                    <span>Емейл</span>
                                    <span>Телефон</span>
                                    <span>Кампанія</span>
                                </li>
                            </ol>
                        </div>

                        <p class="mt-4 text-gray-500 dark:text-gray-400 text-sm leading-relaxed">
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script src="{{asset('js/script.js')}}">

</script>
</body>
</html>
