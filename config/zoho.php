<?php

return [
    'client_id' => env('ZOHO_CLIENT_ID', 'debug'),
    'client_secret' => env('ZOHO_CLIENT_SECRET', 'debug'),
    'redirect_uri' => env('ZOHO_REDIRECT_URI', 'debug'),
    'code' => env('ZOHO_CODE', 'debug'), // 'authorization_code'
    'grant_type' => 'authorization_code',
    'auth_url' => 'https://accounts.zoho.com/oauth/v2/token',
    'coql_url' => 'https://www.zohoapis.com/crm/v5/coql',
    'refresh_token' => env('ZOHO_REFRESH_TOKEN', 'debug'),
    'insert_url' => 'https://www.zohoapis.com/crm/v5/Contacts',
];